new WOW().init();

//fixed head js
/*
jQuery(window).scroll(function(){
    if ($(window).scrollTop() >= 5) {
       jQuery('nav').addClass('fixed-header');
    }
    else {
       jQuery('nav').removeClass('fixed-header');
    }
});
*/




//input zoom js
document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
}, false);




//first letter cap

jQuery('.firstCap').on('keypress', function (event) {
    var $this = jQuery(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});


/*testmonial slider*/

$(document).ready(function () {
    var owl = $('.owl_slider .owl-carousel');
    owl.owlCarousel({
        items: 2,
        loop: true,
        margin: 27,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true
    });
    $('.play').on('click', function () {
        owl.trigger('play.owl.autoplay', [1000])
    })
    $('.stop').on('click', function () {
        owl.trigger('stop.owl.autoplay')
    })
})

/*private lesson slider*/

$(document).ready(function () {
    var owl = $('.side_by_rt .owl-carousel');
    owl.owlCarousel({
        items: 1,
        loop: false,
        margin: 20,
        lazyLoad: true,
        nav: true,
        navText: ['', ''],
        rewindNav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true
    });

})



/*private lesson slider*/

$(document).ready(function () {
    var owl = $('.privte_testimonal .owl-carousel');
    owl.owlCarousel({
        items: 2,
        loop: false,
        margin: 30,
        lazyLoad: true,
        nav: true,
        navText: ['', ''],
        rewindNav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true
    });

})

/*vertical lesson slider*/

$(document).ready(function () {
    var owl = $('.blog_rt_bx .owl-carousel');
    owl.owlCarousel({
        items: 1,
        loop: false,
        margin: 30,
        lazyLoad: true,
        nav: true,
        navText: ['', ''],
        rewindNav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        animateOut: 'slideOutUp',
        animateIn: 'slideInUp',
    });

})


/*vertical slider*/
$('.carousel .vertical .item').each(function () {
    var next = $(this).next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    for (var i = 1; i < 2; i++) {
        next = next.next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo($(this));
    }
});






/*one page scroll*/

$(document).ready(function () {
    $(document).on("scroll", onScroll);

    //smoothscroll
    $('#menu-center ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top + 2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event) {
    var scrollPos = $(document).scrollTop();
    $('#menu-center a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#menu-center ul li a').removeClass("active");
            currLink.addClass("active");
        } else {
            currLink.removeClass("active");
        }
    });
}

//according js

jQuery(function() {
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
});

//custom scroll//

(function(jQuery){
			jQuery(window).on("load",function(){
				
				jQuery(".das_right_bx").mCustomScrollbar({
					theme:"minimal"
				});
                
               jQuery(".dashboard_menu").mCustomScrollbar({
					theme:"minimal"
				});
                
              
				
			});
		})(jQuery); 